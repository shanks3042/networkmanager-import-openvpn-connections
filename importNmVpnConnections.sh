 #!/bin/bash

: '
  author: shanks3042
'
 
folder=$1
username=$2
password=$3
dns=$4
password_unencrypted=$5

echo "$1 $2 $3 $4 $5"

MIN_ARGS=3
MAX_ARGS=5

if [ $# -lt $MIN_ARGS ]; then
  echo "usage: ./importNmConnections.sh path-to-files vpn-username vpn-password [vpn-dns] [password-flags (unencrypted=0, encrypted=1) [0 or 1] ]"
  exit -1
fi

for file in $folder/*.ovpn; do

  echo $file
  filename=${file%.*}
  filename=${filename##*/}
  
  
  connection_exists=$(nmcli connection show $filename)
  
  
  #only import a connection if there is none with the same name
  if [ $? != 0 ]; then

    #import the vpn connection
    connection_name=$(nmcli connection import type openvpn file $file)  


    #extract the connection name (string between '')
    
    #connection_name=${connection_name%"'"*}
    #connection_name=${connection_name##*"'"}
    
    #check if you need this line or the 2 lines above that are commented out - adjust to your needs
    connection_name=$filename
    
    #echo "connection name = $connection_name"  
    #edit vpn dns
    if [ $# -gt $MIN_ARGS ]; then
      nmcli connection modify $connection_name ipv4.dns "$dns"
    fi

    #add vpn username and make connection available for everyone (password will be saved unencrypted)
    #if you want to store your password encrypted then remove password-flags = 0 or change it to password-flags = 1
    if [ $# -ge $MAX_ARGS ] && [ $password_unencrypted==0 ] ; then
      nmcli connection modify $connection_name +vpn.data "username=$username,password-flags = 0"
    elif [ $# -ge $MAX_ARGS ] ; then
      nmcli connection modify $connection_name +vpn.data "username=$username,password-flags = 1"
    fi
    
    nmcli connection modify $connection_name vpn.secrets "password=$password"
    
  fi
    
done
